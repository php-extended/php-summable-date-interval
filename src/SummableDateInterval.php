<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-summable-date-interval library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

/**
 * SummableDateInterval class file.
 * 
 * This class sums and diffs date intervals on the same scale. For the purpose
 * of this class, the following is accepted as rules :
 * - every year has 12 months
 * - every month has 31 days
 * - every day has 24 hours
 * - every hour has 60 minutes
 * - every minute has 60 seconds
 * - every second has 100 000 microseconds
 * 
 * The value days will keep track of the exact number of days regardless of the
 * wrong calculations made by the day/month/year combination. This is not an
 * absolute value, but a floating point relative value of the number of days
 * that this interval represents.
 * 
 * All time units are supposed to be positive. If the invert is also positive,
 * that 
 * 
 * @author Anastaszor
 * @psalm-suppress InaccessibleProperty
 */
class SummableDateInterval extends DateInterval implements Stringable
{
	
	/**
	 * formating string like ISO 8601 (PnYnMnDTnHnMnS).
	 */
	public const INTERVAL_ISO8601 = 'P%yY%mM%dDT%hH%iM%sS';
	
	/**
	 * Creates a new summable date interval from the given standard date interval.
	 * 
	 * @param DateInterval $interval
	 * @return SummableDateInterval
	 * @throws Exception if the value is not parseable
	 */
	public static function createFromInterval(DateInterval $interval) : SummableDateInterval
	{
		$format = $interval->format(SummableDateInterval::INTERVAL_ISO8601);
		$sdi = new self($format);
		$sdi->invert = $interval->invert;
		
		return $sdi;
	}
	
	/**
	 * Creates a new summable date interval from the difference from i1 over i2.
	 * 
	 * @param DateTimeInterface $dti1
	 * @param DateTimeInterface $dti2
	 * @return SummableDateInterval
	 * @throws Exception if the value is not parseable
	 */
	public static function createFromDiff(DateTimeInterface $dti1, DateTimeInterface $dti2) : SummableDateInterval
	{
		return static::createFromInterval($dti1->diff($dti2));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function __toString() : string
	{
		$sReturn = 'P';
		
		if(0 < $this->y)
		{
			$sReturn .= ((string) $this->y).'Y';
		}
		
		if(0 < $this->m)
		{
			$sReturn .= ((string) $this->m).'M';
		}
		
		if(0 < $this->d)
		{
			$sReturn .= ((string) $this->d).'D';
		}
		
		$hReturn = '';
		if(0 < $this->h)
		{
			$hReturn .= ((string) $this->h).'H';
		}
		
		if(0 < $this->i)
		{
			$hReturn .= ((string) $this->i).'M';
		}
		
		if(0 < $this->s)
		{
			$hReturn .= ((string) $this->s).'S';
		}
		
		if('P' === $sReturn)
		{
			return 'PT0S';
		}
		
		return $sReturn.(empty($hReturn) ? '' : 'T'.$hReturn);
	}
	
	/**
	 * Adds the given interval from this interval.
	 * 
	 * @param DateInterval $interval
	 */
	public function add(DateInterval $interval) : void
	{
		// STEP 1 : do the additions
		$direction = ((int) $this->invert === (int) $interval->invert) ? 1 : -1;
		// both intervals are in the same direction, add their values
		// both intervals are in different directions, sub their values
		$this->y += ($direction * (int) $interval->y);
		$this->m += ($direction * (int) $interval->m);
		$this->d += ($direction * (int) $interval->d);
		$this->h += ($direction * (int) $interval->h);
		$this->i += ($direction * (int) $interval->i);
		$this->s += ($direction * (int) $interval->s);
		$this->f += (((float) $direction) * (float) $interval->f);
		$this->days = ((int) $this->days) + ($direction * (int) $interval->days);
		
		// STEP 2 : normalize the size of the values
		if($this->invert)
		{
			$this->invert();
		}
		// here the values should always be positive, except for the year at
		// the end of the process
		$this->bubbleOverflowsToYear();
		if(0 > $this->y)
		{
			$this->invert();
			$this->bubbleOverflowsToYear();
		}
	}
	
	/**
	 * Substracts the given interval from this interval.
	 * 
	 * @param DateInterval $interval
	 */
	public function sub(DateInterval $interval) : void
	{
		$newInterval = clone $interval;
		$newInterval->invert = (int) (bool) $newInterval->invert;
		$this->add($newInterval);
	}
	
	/**
	 * Internally inverts the representation of this interval.
	 */
	protected function invert() : void
	{
		$this->invert = (int) !((bool) $this->invert);
		$this->y = -$this->y;
		$this->m = -$this->m;
		$this->d = -$this->d;
		$this->h = -$this->h;
		$this->i = -$this->i;
		$this->s = -$this->s;
		$this->f = -$this->f;
	}
	
	/**
	 * Transforms the remainders of the lesser units to the upper units, until
	 * no values are overflown to the negative.
	 */
	protected function bubbleOverflowsToYear() : void
	{
		while(0 > $this->f)
		{
			$this->s--;
			$this->f += ((float) 1000000);
		}
		
		while(0 > $this->s)
		{
			$this->i--;
			$this->s += 60;
		}
		
		while(0 > $this->i)
		{
			$this->h--;
			$this->i += 60;
		}
		
		while(0 > $this->h)
		{
			$this->d--;
			$this->h += 24;
		}
		
		while(0 > $this->d)
		{
			$this->m--;
			$this->d += 31;
		}
		
		while(0 > $this->m)
		{
			$this->y--;
			$this->m += 12;
		}
	}
	
}
