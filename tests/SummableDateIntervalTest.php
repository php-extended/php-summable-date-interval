<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-summable-date-interval library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PHPUnit\Framework\TestCase;

/**
 * SummableDateIntervalTest test file.
 * 
 * @author Anastaszor
 * @covers \SummableDateInterval
 *
 * @internal
 *
 * @small
 */
class SummableDateIntervalTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var SummableDateInterval
	 */
	protected SummableDateInterval $_object;
	
	public function testToString() : void
	{
		$this->assertEquals('P1DT1H', $this->_object->__toString());
	}
	
	public function testZeroSeconds() : void
	{
		$this->assertEquals('PT0S', (new SummableDateInterval('PT0S'))->__toString());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new SummableDateInterval('P1DT1H');
	}
	
}
