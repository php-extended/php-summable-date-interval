# php-extended/php-summable-date-interval
A library that creates summable and diffable date intervals.

![coverage](https://gitlab.com/php-extended/php-summable-date-interval/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-summable-date-interval/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-summable-date-interval ^8`


## Basic Usage

This library may be used the following way : 

```php

$dt1 = \DateTime::createFromFormat('<put your string value there>');
$dt2 = \DateTime::createFromFormat('<put another str value there>');

$interval = \SummableDateInterval::createFromDiff($dt1, $dt2);

$dt3 = new \DateTime('<put another str value here>');
$dt4 = new \DateTime();

$dtinterval = $dti = $dt3->diff($dt4);

// then add them together

$interval->add($dtinterval);
// or
$interval->sub($dtinterval);

// you can also use

$interval2 = \SummableDateInterval::createFromInterval($dtinterval);

// then, you can reinject it into a \DateTime :

$future = $dt4->add($interval);

```


## License

MIT (See [license file](LICENSE)).
